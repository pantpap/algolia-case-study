import {Component, Inject, forwardRef, OnInit} from '@angular/core';
import { NgAisIndex, TypedBaseWidget, NgAisInstantSearch } from 'angular-instantsearch';
import connectRefinementList, {
  RefinementListWidgetDescription,
  RefinementListConnectorParams
} from 'instantsearch.js/es/connectors/refinement-list/connectRefinementList';

@Component({
  selector: 'app-refinement-list',
  templateUrl: './refinement-list.component.html',
  styleUrls: ['./refinement-list.component.scss']
})
export class RefinementListComponent extends TypedBaseWidget<RefinementListWidgetDescription, RefinementListConnectorParams> implements OnInit {
  parentIndex!: NgAisIndex;
  instantSearchInstance!: NgAisInstantSearch;
  constructor(
    @Inject(forwardRef(() => NgAisInstantSearch))
    public instantSearchParent: any) {
    super('RefinementListComponent')
   }

  ngOnInit(): void {
    this.createWidget(connectRefinementList, {
      // attribute:'childSkus.brand.displayName'
      attribute:'*'
    });
    console.log(this.state); // this is undefined so i get console errors
  }

}
