import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgAisModule } from 'angular-instantsearch';

import { AppComponent } from './app.component';
import { RefinementListComponent } from './refinement-list/refinement-list.component';

@NgModule({
  declarations: [
    AppComponent,
    RefinementListComponent
  ],
  imports: [
    BrowserModule,
    NgAisModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
