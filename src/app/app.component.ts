import { Component } from '@angular/core';
import algoliasearch from 'algoliasearch/lite';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  config = {
    searchClient: algoliasearch('UQJ92U4LWY', '77a8c094e7464da75163c48b76389f01'),
    indexName: 'test',
    routing: true
  };
  title = 'algolia-case-study';

  tranformRefinementItems(items: any[]): any{
    console.log(items);
  }
}
